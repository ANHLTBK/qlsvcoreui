import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConfigserviceService} from '../Shared/configservice.service';
import {UserDeleteRequest} from '../shared/request/user-delete-request';
import {Observable} from 'rxjs';
import {LocalStorage} from '../common/extention';
import {StudentModel} from '../shared/models/studentModel';
import { StudentDeleteRequest } from '../Shared/request/student-delete-request';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  baseUrl = '';

  constructor(private http: HttpClient, private configService: ConfigserviceService) {
    this.baseUrl = configService.getApiURI();
  }

  GetStudent(): Observable<StudentModel[]> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<StudentModel[]>(this.baseUrl + 'Student/GetAllSudent', httpOptions);
  }

  DeleteStudent(request: StudentDeleteRequest): Observable<any> {
    // tslint:disable-next-line:no-debugger
    debugger;
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    // tslint:disable-next-line:no-debugger
    debugger;
    return this.http.post(this.baseUrl + 'Student/DeleteStudent', request, httpOptions);
  }

  AddStudent(request: StudentModel): Observable<any> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.baseUrl + 'Student/AddStudent', request, httpOptions);
  }

  UpDataStudent(request: StudentModel): Observable<any> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.baseUrl + 'Student/UpDateStudent', request, httpOptions);
  }

  getByIdStudent(id: string): Observable<StudentModel> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<StudentModel>(this.baseUrl + 'Student/GetAllSudent' + '/' + id, httpOptions);
  }
}
