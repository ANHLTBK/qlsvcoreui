import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConfigserviceService} from '../Shared/configservice.service';
import {BehaviorSubject} from 'rxjs';
import {switchMap} from 'rxjs/internal/operators';
import {LoginResponse} from '../shared/models/login.model';

@Injectable()
export class UserLoginService {
  handleError = 'lỗi';
  baseUrl = '';
  private _authNavStatusSource = new BehaviorSubject<boolean>(false);
  authNavStatus$ = this._authNavStatusSource.asObservable();
  private loggedIn = false;

  constructor(private http: HttpClient, private configService: ConfigserviceService) {
    this.baseUrl = configService.getApiURI();
  }

  login(userName, password) {
    debugger;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    debugger;
    return this.http.post<LoginResponse>(this.baseUrl + 'UserLogin/Login',
      JSON.stringify({userName, password}), httpOptions
    );
  }
}
