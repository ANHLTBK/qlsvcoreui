import {Injectable} from '@angular/core';
import {ConfigserviceService} from '../Shared/configservice.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../shared/models/userModel';
import {LocalStorage} from '../common/extention';
import {UserDeleteRequest} from '../shared/request/user-delete-request';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = '';

  constructor(private http: HttpClient, private configService: ConfigserviceService) {
    this.baseUrl = configService.getApiURI();
  }

  GetUser(): Observable<User[]> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<User[]>(this.baseUrl + 'User/GetAllUser', httpOptions);
  }

  DeleteUser(request: UserDeleteRequest): Observable<any> {
    debugger;
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    debugger;
    return this.http.post(this.baseUrl + 'User/DeleteUser', request, httpOptions);
  }

  AddUser(request: User): Observable<any> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.baseUrl + 'User/AddOrChange', request, httpOptions);
  }

  UpDataUser(request: User): Observable<any> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.baseUrl + 'User/Update', request, httpOptions);
  }

  getByIdUser(id: string): Observable<User> {
    const token = LocalStorage.getCookie('token_jwt');
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<User>(this.baseUrl + 'User/GetUser' + '/' + id, httpOptions);
  }
}
