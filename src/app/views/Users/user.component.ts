import {Component, Inject, OnInit} from '@angular/core';
import {getStyle, rgbToHex} from '@coreui/coreui/dist/js/coreui-utilities';
import {UserDeleteRequest} from '../../Shared/request/user-delete-request';
import {User} from '../../Shared/models/userModel';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html'
})
export class UserComponent implements OnInit {
  constructor(private userservice: UserService) {
  }

  UserModel: User = new User();
  listUserModel: User[] = [];
  userdeleteRequest: UserDeleteRequest = new UserDeleteRequest();

  ngOnInit() {
    this.listUserModel = [];
    this.UserModel = new User();
    this.getAll();
  }


  getAll() {
    // tslint:disable-next-line:no-debugger
    debugger;
    this.userservice.GetUser().subscribe((res: any[]) => {
      // tslint:disable-next-line: no-debugger
      debugger;
      console.log('getuser', res);
      this.listUserModel = res;
    });
  }

  getByIdUesr(id: string) {
    // tslint:disable-next-line:no-debugger
    debugger;
    this.userservice.getByIdUser(id).subscribe((res: any) => {
      this.UserModel = res;
    });

  }

  onDelete(userId: any) {
    const confirmResult = confirm('are you sure ');
    if (confirmResult) {
      // tslint:disable-next-line:no-debugger
      debugger;
      this.userdeleteRequest.userID = userId;
      this.userservice.DeleteUser(this.userdeleteRequest).subscribe(response => {
        // tslint:disable-next-line:no-debugger
        debugger;
        this.getAll();
        this.resetForm();

      });
    }
  }

  resetForm() {
    this.UserModel = new User();
  }

  onAddUser() {
    this.userservice.AddUser(this.UserModel).subscribe((res: any) => {
      if (res != null) {
        alert('them thanh cong');
        this.getAll();
      }
    });
  }

  onUpData() {
    this.userservice.UpDataUser(this.UserModel).subscribe((res: any) => {
      this.UserModel = res;
      console.log('update', res);
      if (res != null) {
        alert('UpDate thanh cong');
      }
      this.getAll();
    });
  }

  openFormUpdate(item: User) {
    this.UserModel = item;
  }

}
