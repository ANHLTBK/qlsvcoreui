// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UserComponent } from './user.component';
import { TypographyComponent } from './typography.component';

// Theme Routing
import { UserRoutingModule } from './user-routing.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule
  ],
  declarations: [
    UserComponent,
    TypographyComponent
  ]
})
export class UserModule { }
