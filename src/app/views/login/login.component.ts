import {Component, OnInit} from '@angular/core';
import {Credentials} from '../../Shared/models/credentials.interface';
import {UserLoginService} from '../../service/user-login.service';
import {Router} from '@angular/router';
import {LocalStorage} from '../../common/extention';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  errors;
  credentials: Credentials = new Credentials();
  ngOnInit() {
  }
  constructor(private userService: UserLoginService, private router: Router) {
  }

  login({value, valid}: { value: Credentials, valid: boolean }) {
    this.errors = '';
    if (!valid) {
      return;
    }

    this.userService.login(value.UserName, value.password).subscribe(
      result => {
        // tslint:disable-next-line:no-debugger
        debugger;
        if (result) {
          console.log('token', result);
          LocalStorage.setCookie('token_jwt', result.token, 5);
          this.router.navigate(['dashboard']);
        }
      },
      error => this.errors = error);
  }
}
