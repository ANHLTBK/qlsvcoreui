// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Theme Routing
import {FormsModule} from '@angular/forms';
import { StudentRoutingModule } from './student-routing.module';
import { StudentComponent } from './student.component';

@NgModule({
  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule
  ],
  declarations: [
    StudentComponent
  ]
})
export class StudentModule { }
