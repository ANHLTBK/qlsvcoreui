import { Component, OnInit } from '@angular/core';
import { StudentModel } from '../../Shared/models/studentModel';
import { StudentDeleteRequest } from '../../Shared/request/student-delete-request';
import { StudentService } from '../../service/student.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html'
})
export class StudentComponent implements OnInit {
  constructor(private studentService: StudentService) { }

  Studentmodel: StudentModel = new StudentModel();
  Studentmodels: StudentModel[] = [];
  studentRequest: StudentDeleteRequest = new StudentDeleteRequest();


  ngOnInit() {
  }
  getAll() {
    this.studentService.GetStudent().subscribe((res: any[]) => {
      this.Studentmodels = res;
    });
  }
  onDeleteStudent(StudentId: any) {
    const confirmResult = confirm('are you sure ');
    if (confirmResult) {
    this.studentRequest.StudentID = StudentId;
    // tslint:disable-next-line: deprecation
    this.studentService.DeleteStudent(this.studentRequest).subscribe(Response => {
      this.getAll();
      this.resetForm();
    });
  }
  }
  resetForm() {
    this.Studentmodel = new StudentModel();
  }
  onAddStudent() {
    this.studentService.AddStudent(this.Studentmodel).subscribe((res: any) => {
      if (res != null) {
        alert('Thêm thành Công');
      }
      this.getAll();
    });
  }
  onUpdate() {
    this.studentService.UpDataStudent(this.Studentmodel).subscribe((res: any) => {
      this.Studentmodel = res;
      if (res != null) {
        alert('upadate thành công');
      }
      this.getAll();
    });
  }
  openFormUpdate(item: StudentModel) {
    this.Studentmodel = item;
  }
}
