// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Theme Routing
import {FormsModule} from '@angular/forms';
import { SchoolRoutingModule } from './school-routing.module';
import { SchoolComponent } from './school.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SchoolRoutingModule
  ],
  declarations: [
      SchoolComponent
  ]
})
export class SchoolModule { }
