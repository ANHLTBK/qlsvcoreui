import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchoolComponent } from './school.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'School'
    },
    children: [
      {
        path: '',
        redirectTo: 'schools'
      },
      {
        path: 'schools',
        component: SchoolComponent,
        data: {
          title: 'School'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolRoutingModule {}
