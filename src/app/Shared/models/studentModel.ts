export class StudentModel {
  id: string;
  name: string;
  mssv: string;
  address: string;
  email: string;
  phone: string;
}
